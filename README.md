# ZSHELLECO - a z-shell configuration repository

## How to use

### 1. Go to you configuration folder and clone this repository

```
user$ cd ~/.config
user$ git clone git@gitlab.com:drbeco/zshelleco.git zsh
```

### 2. Be certain that you have _zshell_ installed

```
user$ sudo apt-get install zsh
```

### 3. Change your `ZDOTDIR` to point to your new configuration folder

Edit (as root) the following file:

```
user$ sudo vi /etc/zsh/zshenv
```

Then add this line at the bottom:

```
export ZDOTDIR="$HOME/.config/zsh/"
```

### 4. (Optional/Recommended) Make zsh your _default_ shell interpreter

First be sure you have the right name and path looking at the list of valid shells:

```
user$ cat /etc/shells
```

Then change your default shell with:

```
user$ chsh -s /bin/zsh user
```

where _user_ is your _username_ (login).

If you don't want to change the default shell right away, you can do this later at any time. Meanwhile, when you want to run _zshell_, start a terminal with your current default shell and just type:

```
user$ zsh
```

and that is it!

Enjoy


## 5. Shell log and logrotate

### 5.a.1. (Optional/Recommended) Save all your history to a log file

First create the folder that will contain the files.

```
user$ sudo mkdir /var/log/uhistory
```

Be sure others can read and execute (rx) the new folder owned by root.

Create a file to adjust permissions with the commands bellow. Be aware to change `$USER` with your real username.

```
user$ sudo touch /var/log/uhistory/$USER-uhistory.log
user$ sudo setfacl -m u:$USER:rwx /var/log/uhistory/$USER-uhistory.log
```

### 5.a.2. Another option to log (instead of 5.a.1.)

Pipe your command to the `shelllog` script. 

* Move the `shelllog` script and its needed python library `libpython2.7.so.1.0` to the `/usr/local/bin/` folder

* create the `/var/log/uhistory/` folder with `mkdir /var/log/uhistory`

* Set the _sticky bit_ of the script, allowing it to write to `/var/log/` with the command `chmod u+s shelllog`

* Set your `PROMPT_COMMAND` in the `.zshrc` file to:

```
export PROMPT_COMMAND='echo $USER:$(pwd) $(fc -lt"$HISTTIMEFORMATLOG" -1) | /usr/local/bin/shelllog'
```

### 5.b. Set a log rotate configuration

Create also a log file for the root user. You don't need to adjust any permissions for this file, obviously.

```
user$ touch /var/log/uhistory/root-uhistory.log
```

You can now uncomment the line

```
export PROMPT_COMMAND='echo $USER:$(pwd) $(fc -lt"$HISTTIMEFORMATLOG" -1) >> /var/log/uhistory/$USER-uhistory.log' 
```

Now all your commands will be saved as a log file. It is a good idea to include them in a logrotate schedule. You can use the configuration file bellow to setup your logrotate:

```
# Created by Dr. Beco, December 2017
# This is the file /etc/logrotate.d/user-history
# Description: logrotate parameters for all users in /var/log/uhistory in the format: $USER-history.log

/var/log/uhistory/*-uhistory.log
{
    monthly
    rotate 36
    #weekly
    #rotate 156
    #size 100k
    #dateext
    compress
    delaycompress
    missingok
    notifempty
    copytruncate
    nomail
    create 640
}

```


---

* Dr. Beco
* 2019-10-05
* rcb@beco.cc

