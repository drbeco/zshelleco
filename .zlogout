# drbeco 2017-04-29 - 2021-07-15 - 2023-06-17
# Marvin and Draco
#
# ~/.config/zsh/.zlogout: executed by zshell when login shell exits.
#
# ATTENTION: only LOGIN SHELL 

# when leaving the console clear the screen to increase privacy

if [[ "$SHLVL" = "1" ]] ; then
    [[ -x /usr/bin/clear_console ]] && /usr/bin/clear_console -q
fi

#drbeco 2023-06-17, 2019-10-08, 2016-07-16, 2015-06-24 Draco, Marvin
source /usr/local/bin/ssh-stop --verbose

