# Dr Beco 2023-06-16, 2019-05-23, 2019-10-07
#
# ~/.zprofile: executed by zshell interpreter for login shells.
#
# This file run once for each login, ssh (like draco) or boot (like marvin)
# This file do not execute for a second shell window on the same login

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#not umask 0022

# set PATH so it includes user's private bin if it exists
# original PATH="/usr/local/bin:/usr/bin:/bin:/usr/games"
if [[ -d "$HOME/bin" ]] ; then
    PATH="$HOME/bin:$PATH"
fi
PATH="/usr/local/games:/usr/local/sbin:/usr/sbin:/sbin:$PATH"

#drbeco 2022-10-28 rust with rustup
export PATH="$HOME/.rustup/shims:$PATH"

# Turn off messages
#mesg n

## Palavra magica
#echo
#if ! pass_akernaak ; then
#    killall -KILL -u "$USER"
#fi

## Calendario
echo
cal `date +"%m"` `date +"%Y"`
#calendar -A10

#2014-08-12 fortune de /etc/profile para ~/.profile
case $- in
*i* )  # We're interactive
    echo
    /usr/games/fortune fortunes fortunes2 linuxcookie
    #/usr/games/fortune computers linuxcookie linux debian-hints motivational_cookies riddles science startrek literature humorists definitions fortunes wisdom work zippy
    echo
esac

# Dr. Beco, 20160416.001616
qualcard -s
echo

#Disk quota drbeco 20201026

# Logins
# loginlog
# echo You logged in "$(loginlog.sh -l) days this semester"
# echo You are in a "$(loginstreak.sh -q)" days streak'!'
# echo

## Akernaak
echo Akernaak grita $(/usr/games/akernaak -p)'!!'
# export AKERNAAK=$(/usr/games/akernaak -p)
# echo Akernaak grita $AKERNAAK'!!'
echo

################################################################

### starting or just linking ssh-agent
source /usr/local/bin/ssh-start --verbose

##desliga o touchpad
#synclient TouchpadOff=1
# /usr/local/bin/touchpadoff.sh
# /home/beco/bin/touchpad off

## Steam Tray Icon context menu bug
#export STEAM_RUNTIME_PREFER_HOST_LIBRARIES=0

##Troca ESC por Caps-Lock, by Dr. Beco 2017-06-19
#xmodmap ~/.vim/caps-esc-swap.map

