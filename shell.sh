#!/bin/sh
# This file is to be used if your system do not respect the passwd field for default shell (like KDE/Konsole config)
# Move this file to /usr/local/bin
# and set your Konsole configuration to use as shell the command: /usr/local/bin/shell.sh
# Change your shell with chsh command as you with, and your option will be respected.

PSHELL=$(getent passwd $USER|cut -d: -f7)

eval "$PSHELL"

