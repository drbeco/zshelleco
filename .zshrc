# By drbeco, 2023-05-16, 2019-10-05
#
# Z-Shell rc file
#
# This file reads for each login or new shell window

# ... or force ignoredups and ignorespace
setopt EXTENDED_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS
#setopt HIST_IGNORE_ALL_DUPS
#setopt HIST_SAVE_NO_DUPS

# append to the history file, don't overwrite it and autocd with only .. drbeco 20230617
setopt appendhistory autocd

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
# My average: 8.5 months, 5000 commands
#--------------------------------------
# 588 commands per month
# 19.6 commands per day
# 0.82 commands per hour
# 1.22 hour per command
# 7000 commands per year
#--------------------------------------
# 4007 comandos em 65 dias
# 61.65 comandos por dia
# 1850 comandos por mes
# 22200 comandos por ano
# 2,57 comandos por hora
# 1 comando cada 23min
# 33300 comandos em 3 semestres
# 11100 comandos por semestre

# memory size:
HISTSIZE=50000
# hist file size:
SAVEHIST=100000
# hist file location:
HISTFILE=~/.config/zsh/zhistfile
# time stamp
export HISTTIMEFORMATLOG='%F %T '

# allow comments in interactive shell command lines
setopt interactivecomments
# vim mode
bindkey -v
# reverse search, by Dr Beco
bindkey '^R' history-incremental-search-backward
# back and forward a word with control+arrows
bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word

zstyle :compinstall filename '$HOME/.config/zsh/.zshrc'
autoload -Uz compinit
compinit -d $HOME/.cache/zsh/zcompdump-$ZSH_VERSION

eval "`dircolors -b`" #export LS_COLORS='bla bla'
export LS_OPTIONS='--color=auto --time-style=long-iso'
export COLOR_OPTIONS='--color=auto' # grep and others
# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'
export KEYTIMEOUT=1

set PROMPT_SUBST

# Simulates Bash PROMPT_COMMAND running before each command
# Save all history to a var/log/uhistory directory
# To enable global log saving (recommended with logrotation)
# first create $ mkdir /var/log/uhistory
# then $ touch $USER-uhistory.log
# and finally adjust permissions with $ setfacl -m u:$USER:rwx /var/log/uhistory/$USER-uhistory.log
#
#export PROMPT_COMMAND='echo $USER:$(pwd) $(fc -lt"$HISTTIMEFORMATLOG" -1) >> /var/log/uhistory/$USER-uhistory.log'
export PROMPT_COMMAND='echo $USER: $(pwd) $(fc -lt"$HISTTIMEFORMATLOG" -1) | /usr/local/bin/shelllog'
#export PROMPT_COMMAND=''

# make less more friendly for non-text input files, see lesspipe(1)
[[ -x /usr/bin/lesspipe ]] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${slackware_chroot:-}" ] && [ -r /etc/slackware_chroot ]; then
    slackware_chroot=$(cat /etc/slackware_chroot)
fi

# load the z-aliases file
if [[ -f ~/.config/zsh/.zsh_aliases ]] ; then
    . ~/.config/zsh/.zsh_aliases
fi

# load the z-functions file
if [[ -f ~/.config/zsh/.zsh_functions ]] ; then
    . ~/.config/zsh/.zsh_functions
fi

# set a function to run each KEYMAP change
zle -N zle-keymap-select

# drbeco 2023-06-11, call _setprompt() to set the prompt
_setprompt

############################################
# drbeco, 2023-06-17, 2019-05-23 - ~/.config/zsh/.zshrc 

#Slackware Package
#SLACKEMAIL="your@email"
#SLACKFULLNAME="Your Name"
export SLACKEMAIL SLACKFULLNAME

#export SLACKSIGN_KEYID=94H897G9
#export SLACKCOMMIT_SIGN_TAGS=yes
#export GIT_AUTHOR_NAME=$SLACKFULLNAME
#export GIT_AUTHOR_EMAIL=$SLACKEMAIL
#export GIT_COMMITTER_NAME=$SLACKFULLNAME
#export GIT_COMMITTER_EMAIL=$SLACKEMAIL

# drbeco 20230618 moved to /etc/zprofile
# move .vimrc to ~/.config/vim/vimrc
# export VIMINIT="source ~/.config/vim/vimrc"

#editor para CRON
export EDITOR=vim

#turn off messages 2014-06-03, by drbeco
#mesg n

### ------------------- GPG  and GPG TTY
# drbeco 2014-11-09 --- created gpg key for email@domain, TTY 2017-11-09
# export GPGKEY=ABCDEF09
#export GNUPGHOME=/media/usb/
export GPG_TTY=$(tty)
## Refresh gpg-agent tty in case user switches into an X session
gpg-connect-agent updatestartuptty /bye >/dev/null

# drbeco, 20150820.030236
# Change CTRL-S from tty suspend to forward search instead of suspend (opposite of CTRL+R)
[[ $- == *i* ]] && stty -ixon

# Make less keep the screen when exiting --- Dr Beco, 2016-02-07
export LESS="-X"

# Set mail folder drbeco 2016-05-02
export MBOX=~/.mbox

# Dr. Beco 2019-10-08, The new shell
export SHELL=/bin/zsh

## drbeco 2023-06-11 2021-07-16, XDG_RUNTIME used for ssh -X (or -Y) X11forward
export XDG_RUNTIME_DIR="/tmp/runtime-$USER"

####################################################################################
# User agent
# now at ~/.profile with a script of its own: source /usr/local/bin/ssh-start

# Binding ALT-R do CTRL-R with already entered text
#bindkey '"\er":"\C-a\C-r\C-y\C-r"' # alt-r = ctr-a ctr-r ctr-y ctr-r

# DOT FILES drbeco 20230617
export DOTBASH=".profile .bashrc .bash_aliases .bash_functions .bash_logout"
export DOTZSH=".zprofile .zshrc .zsh_aliases .zsh_functions .zlogout"

###########
# Usuarios
#export USUARIOS=$(find /home -nowarn -type d -maxdepth 1 -name "*" -printf "%f\n"|grep -v "\.\|home\|beco\|admin\|hydra\|draco\|marvin\|ftp\|lost+found\|git"|sort)
#export USUPROG1=$(for u in $USUARIOS ; do grep -q "programa1" /home/$u/.plan && echo $u ; done)
#export USUPROG2=$(for u in $USUARIOS ; do grep -q "programa2" /home/$u/.plan && echo $u ; done)
#export USUPROG3=$(for u in $USUARIOS ; do grep -q "programa3" /home/$u/.plan && echo $u ; done)
#export USUPROGA=$(for u in $USUARIOS ; do grep -q "programauto" /home/$u/.plan && echo $u ; done)
#export USUIA=$(for u in $USUARIOS ; do grep -q "Courses:.*ia" /home/$u/.plan && echo $u ; done)
#export USUMOINS=$(for u in $USUARIOS ; do grep -q "monitor\|instrutor" /home/$u/.plan && echo $u ; done)
#export USUMON=$(for u in $USUARIOS ; do grep -q "monitor" /home/$u/.plan && echo $u ; done)
#export USUINS=$(for u in $USUARIOS ; do grep -q "instrutor" /home/$u/.plan && echo $u ; done)
#export USUDIV=$(for u in $USUARIOS ; do grep -q "estagio\|Courses:.*tcc\|Courses:.*ic[^au]" /home/$u/.plan && echo $u ; done)
#export USUEX=$(for u in $USUARIOS ; do grep -q "ex_aluno" /home/$u/.plan && echo $u ; done)
# USUMINI18="acf cmdof dggd egds gdad gso gtrda hlcdn hrdsl japds jmdlj jvdos kldfs mld mmm ofrdoj pwods ram vagr vapj yvcds"
# USUPROF="beco zato campospg"

